package com.classpath.orders.controller;

import java.util.Map;
import java.util.Set;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.classpath.orders.model.Order;
import com.classpath.orders.model.OrderDto;
import com.classpath.orders.service.OrderService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
public class OrderRestController {
	
	private final OrderService orderService;
	
	
	@GetMapping 
	public Map<String, Object> fetchAllOrders(
			@RequestParam(name="page", defaultValue = "0", required = false) int page,
			@RequestParam(name="size", defaultValue = "10", required = false) int size,
			@RequestParam(name="order", defaultValue = "asc", required = false) String direction,
			@RequestParam(name="field", defaultValue = "price", required = false) String property
			){
		return this.orderService.fetchAllOrders(page, size, direction, property);
	}
	
	@GetMapping("/{id}")
	public Order fetchOrderById(@PathVariable("id") long orderId) {
		return this.orderService.fetchOrderById(orderId);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Order saveOrder(@RequestBody Order order) {
		//workaround
		
		 
		/*
		 * Authentication authentication =
		 * SecurityContextHolder.getContext().getAuthentication(); String emailAddress =
		 * authentication.getName();
		 */
		//User loggedInUser = this.customerRepository.findByEmail(emailAddress).get();
		//loggedInUser.placeOrder(order);
		//User savedUser = this.customerRepository.save(loggedInUser);
		return this.orderService.saveOrder(order);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteOrderById(@PathVariable long id) {
		this.orderService.deleteOrderById(id);
	}
}
