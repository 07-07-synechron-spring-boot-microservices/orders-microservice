package com.classpath.orders.service;

import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.cloud.stream.messaging.Source;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.classpath.orders.event.OrderEvent;
import com.classpath.orders.event.OrderStatus;
import com.classpath.orders.model.Order;
import com.classpath.orders.repository.OrderRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class OrderService {

	private final OrderRepository orderRepository;
	private final WebClient webClient;
	private final Source source;

	//@CircuitBreaker(name="inventoryservice", fallbackMethod = "fallback")
	public Order saveOrder(Order order) {
		
		//invoke the rest api with inventory microservice
		/*
		 * ClientResponse clientResponse = this.webClient.post()
		 * .uri("/api/v1/inventory") .exchange() .block(Duration.of(5,
		 * ChronoUnit.SECONDS));
		 */
		OrderEvent orderEvent = OrderEvent.builder().order(order).status(OrderStatus.ORDER_ACCEPTED).orderTime(LocalDate.now()).build();
		Message<OrderEvent> OrderEvent = MessageBuilder.withPayload(orderEvent).build();
		this.source.output().send(OrderEvent);
		return order;
	}
	
	private Order fallback(Exception exception) {
		System.out.println("Exception while communication with inventory microservice "+ exception.getMessage());
		return Order.builder().build();
	}

	public Map<String, Object> fetchAllOrders(int page, int size, String direction, String property) {

		Sort.Direction sortDirection = direction.equalsIgnoreCase("asc") ? Sort.Direction.ASC : Sort.Direction.DESC;

		Pageable pageRequest = PageRequest.of(page, size, sortDirection, property);

		/*
		 * Authentication authentication =
		 * SecurityContextHolder.getContext().getAuthentication(); String emailAddress =
		 * authentication.getName();
		 */

		Page<Order> pageResponse = this.orderRepository.findAll(pageRequest);

		int totalNumberOfPages = pageResponse.getTotalPages();
		long totalElements = pageResponse.getTotalElements();
		List<Order> data = pageResponse.getContent();

		Map<String, Object> response = new LinkedHashMap<>();

		response.put("pages", totalNumberOfPages);
		response.put("total", totalElements);
		response.put("data", data);

		return response;
	}

	public Order fetchOrderById(long orderId) {
		return this.orderRepository.findById(orderId)
				.orElseThrow(() -> new IllegalArgumentException("invalid order id passed"));
	}

	public void deleteOrderById(long orderId) {
		this.orderRepository.deleteById(orderId);
	}

}
