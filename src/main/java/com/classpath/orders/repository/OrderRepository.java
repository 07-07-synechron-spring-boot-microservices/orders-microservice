package com.classpath.orders.repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.classpath.orders.model.Order;
import com.classpath.orders.model.OrderDto;

@Repository
public interface OrderRepository extends JpaRepository<Order ,Long>{
	
	Page<Order> findByPriceBetween(double min, double max, Pageable page);
	
	Page<Order> findByPriceGreaterThanEqual(double minPrice, Pageable page);
}
