package com.classpath.orders.event;

public enum OrderStatus {
	
	ORDER_ACCEPTED,
	ORDER_PENDING,
	ORDER_FULFILLED,
	ORDER_REJECTED,
	ORDER_CANCELLED

}
