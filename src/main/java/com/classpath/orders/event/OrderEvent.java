package com.classpath.orders.event;

import java.time.LocalDate;

import com.classpath.orders.model.Order;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Builder
@AllArgsConstructor
@Getter
@NoArgsConstructor( access = AccessLevel.PROTECTED)
public class OrderEvent {
	
	private Order order;
	private LocalDate orderTime;
	private OrderStatus status;

}
