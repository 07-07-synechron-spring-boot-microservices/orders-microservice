package com.classpath.orders.util;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class ApplicationRunnerDemo implements CommandLineRunner {
	
	private ApplicationContext applicationContext;
	
	public ApplicationRunnerDemo(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	@Override
	public void run(String... args) throws Exception {
		String[] beanNames = this.applicationContext.getBeanDefinitionNames();
		System.out.println("*****************************************");
		/*
		 * Arrays.asList(beanNames) .stream() //.filter(bean -> bean.contains("user"))
		 * .forEach(bean -> System.out.println(bean));
		 */
		System.out.println("*****************************************");
		
	}
}
