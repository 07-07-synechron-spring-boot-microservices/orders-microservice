package com.classpath.orders.util;

import java.time.ZoneId;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.classpath.orders.model.LineItem;
import com.classpath.orders.model.Order;
import com.classpath.orders.repository.OrderRepository;
import com.github.javafaker.Faker;
import com.github.javafaker.Name;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class BootstrapAppData implements ApplicationListener<ApplicationReadyEvent> {

	// private final PasswordEncoder passwordEncoder;
	private final Faker faker = new Faker();
	private final OrderRepository orderRepository;

	@Value("${app.customerCount}")
	private int orderCount;

	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {

		// imperative style of programming //for(int i = 0; i < 10; i ++)

		// declarative style of programming

		IntStream.range(0, orderCount).forEach(index -> {
			Name customerName = faker.name();

			IntStream.range(0, faker.number().numberBetween(1, 4)).forEach(cId -> {
				Order order = Order.builder().orderDate(
						faker.date().past(4, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
						.build();

				IntStream.range(0, faker.number().numberBetween(1, 4)).forEach(oId -> {
					LineItem lineItem = LineItem.builder().name(faker.commerce().productName())
							.qty(faker.number().numberBetween(2, 5))
							.pricePerUnit(faker.number().randomDouble(2, 300, 800)).build();
					order.addLineItem(lineItem);
				});

				double totalOrderPrice = order.getLineItems().stream()
						.map(lineItem -> lineItem.getQty() * lineItem.getPricePerUnit())
						.reduce((price1, price2) -> price1 + price2).orElse(0d);
				order.setPrice(totalOrderPrice);
				this.orderRepository.save(order);
			});
		});
	}
}
