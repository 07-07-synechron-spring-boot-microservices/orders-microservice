package com.classpath.orders.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import lombok.extern.slf4j.Slf4j;


@Configuration
@Slf4j
public class WebApplicationConfiguration implements WebMvcConfigurer {
	
	/*
	 * @Override public void addCorsMappings(CorsRegistry registry) {
	 * log.info("Adding the cors mapping::"); registry.addMapping("/**")
	 * .allowedOrigins("*") .allowedMethods("GET", "PUT", "POST", "DELETE"); }
	 */

}
