package com.classpath.orders.config;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import com.classpath.orders.repository.OrderRepository;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
class DBHealthIndicator implements HealthIndicator {

	private final OrderRepository orderRepository;
	
	@Override
	public Health health() {
		long records = this.orderRepository.count();
		if(records > 0) {
			return Health.up().withDetail("DB-status", "DB is up and running").build();
		}
		return Health.down().withDetail("DB-status", "DB is down and not running").build();
	}

}
