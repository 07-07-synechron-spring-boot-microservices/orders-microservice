package com.classpath.orders.model;

import java.time.LocalDate;


public interface OrderDto {

	LocalDate getOrderDate();

	double getPrice();

}
